﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Autodesk
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

namespace AutoCAD_app_2021
{
     class methods
    {
        /// <summary>
        /// Вернет искомую коллекцию блоков
        /// </summary>
        /// <param name="trans"></param>
        /// <param name="db"></param>
        /// <param name="search_block_name">Назавание искомого блока "LABEL***"</param>
        /// <returns></returns>
        public static List<ObjectId> search_blocks(Transaction trans, Database db, string search_block_name)
        {
            BlockTable bt = (BlockTable)trans.GetObject(db.BlockTableId, OpenMode.ForRead); // открываем таблицу блоков на чтение
            List<ObjectId> blocks_instances = new List<ObjectId>(); // создаем список который будет содрежать результат поиска
            foreach (ObjectId btrId in bt) // проход по каждому блоку из коллекции db.BlockTableId
            {
                // получаем запись из таблицы блоков и смотрим условие
                BlockTableRecord block = (BlockTableRecord)trans.GetObject(btrId, OpenMode.ForRead);
                if (block.Name.Contains(search_block_name)) // ищем блоки в именамми "LABEL***"
                {
                    // получаем все прямые вставки блока
                    ObjectIdCollection BlockRefs = block.GetBlockReferenceIds(true, true); // коллекция на экземпляры блоков
                    foreach (ObjectId id in BlockRefs) // проход по экземплярам блоков
                    {
                        BlockReference bref = trans.GetObject(id, OpenMode.ForRead) as BlockReference; // получаем block reference по id
                        BlockTableRecord btr = (BlockTableRecord)trans.GetObject(bref.BlockTableRecord, OpenMode.ForRead); // получаем кокретный блок
                        foreach (ObjectId id_Entity in btr) // проход по вложенным в блок элементам
                        {
                            if (id_Entity.ObjectClass.DxfName == "LINE") // ИЩЕМ ЛИНИЮ СИНЕГО ЦВЕТА
                            {
                                Entity entity = (Entity)trans.GetObject(id_Entity, OpenMode.ForRead);
                                if (entity.ColorIndex == 160) // ЕСЛИ ЦВЕТ СИНИЙ - ТО ЧТО ИСКАЛИ ColorIndex == 160 - СИНИЙ ЦВЕТ
                                    if (!blocks_instances.Contains(id)) // не добавлять дубли
                                        blocks_instances.Add(id);

                            }
                        }
                    }
                }
            }
            //только уникальные
            return blocks_instances;
        }

        /// <summary>
        /// Создание слоя
        /// </summary>
        /// <param name="trans"></param>
        /// <param name="db"></param>
        /// <param name="layer"></param>
        public static LayerTableRecord create_layer(Transaction trans, Database db,string name, Editor ed)
        {

            // открываем таблицу слоев документа
            LayerTable layerTbl = trans.GetObject(db.LayerTableId, OpenMode.ForWrite) as LayerTable;
            if (layerTbl.Has(name))
            {
                ed.WriteMessage($"Слой с названием {name} уже сущетсвует. \n");
                return (layerTbl[name].GetObject(OpenMode.ForRead) as LayerTableRecord);

            }
            
            // создаем новый слой и задаем ему имя
            LayerTableRecord layer_tab_rec = new LayerTableRecord();
            layer_tab_rec.Name = name;
            layer_tab_rec.Color = Autodesk.AutoCAD.Colors.Color.FromRgb(255, 0, 0);// КРАСНЫЙ
            // заносим созданный слой в таблицу слоев, сохраняем ID созданной записи слоя
            ObjectId layer = layerTbl.Add(layer_tab_rec);
            // добавляем созданный слой в документ
            trans.AddNewlyCreatedDBObject(layer_tab_rec, true);
            return layer_tab_rec;

        }

        /// <summary>
        /// Создания блока
        /// </summary>
        /// <param name="trans"></param>
        /// <param name="db"></param>
        public static BlockTableRecord create_block(Transaction trans, Database db,string blockName, Editor ed, string atribut_text)
        {
            try
            {
                /*
           Чтобы создать блок и поместить его вхождение на чертеж, необходимо сделать следующее:
           1 - Создать в таблице блоков новую запись(определение блока).
           2 - Добавить в определение блока необходимые геометрические объекты.
           3 - Добавить на чертеж вхождение блока.
           */

                //***
                // ШАГ 1 - создаем новую запись в таблице блоков
                //***

                // открываем таблицу блоков на запись
                BlockTable bt = (BlockTable)trans.GetObject(db.BlockTableId, OpenMode.ForWrite);

                // вначале проверяем, нет ли в таблице блока с таким именем
                // если есть - выводим сообщение об ошибке и заканчиваем выполнение команды
                if (bt.Has(blockName))
                {
                    ed.WriteMessage("\nБлок с именем \"" + blockName + "\" уже существует.");
                    return (bt[blockName].GetObject(OpenMode.ForRead) as BlockTableRecord);
                }

                // создаем новое определение блока, задаем ему имя
                BlockTableRecord block_defenition = new BlockTableRecord();
                block_defenition.Name = blockName;
                block_defenition.Origin = new Point3d(0,0,0);
                bt.UpgradeOpen();// P.S. работает и без этой строки
                // добавляем созданное определение блока в таблицу блоков и в транзакцию,
                // запоминаем ID созданного определения блока (оно пригодится чуть позже)
                ObjectId btrId = bt.Add(block_defenition); // это определение блока в базе данных
                trans.AddNewlyCreatedDBObject(block_defenition, true);

                //***
                // ШАГ 2 - добавляем к созданной записи необходимые геометрические примитивы
                //***          


                #region Примитив текст.
                // создаем текст/ если убрать и оставить только атрибут работать не будет.
                DBText text = new DBText();
                text.Position = new Point3d(0, 0, 0);
                text.Height = 2.5;
                text.TextString = " ";
                // добавляем текст в определение блока и в транзакцию
                block_defenition.AppendEntity(text);
                trans.AddNewlyCreatedDBObject(text, true);
                #endregion

                // Создаём новый AttributeReference
                using (AttributeReference attRef = new AttributeReference())
                {
                    AttributeDefinition attribute = new AttributeDefinition();
                    attribute.SetDatabaseDefaults();
                    attribute.Position = new Point3d(0,0, 0);
                    attribute.Verifiable = true;
                    attribute.Justify = AttachmentPoint.BaseLeft;
                    attribute.Invisible = false;
                    attribute.Height = 2.5;
                    attribute.TextString = atribut_text;
                    attribute.Tag = "attribut_mleader_1";
                    attribute.LockPositionInBlock = false;
                    // добавляем атрибут в определение блока и в транзакцию
                    block_defenition.AppendEntity(attribute);
                    trans.AddNewlyCreatedDBObject(attribute, true);
                }


                
                /*
                //***
                // ШАГ 3 - добавляем вхождение созданного блока на чертеж
                //***

                // открываем пространство модели на запись
                BlockTableRecord ms = (BlockTableRecord)trans.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);

                // создаем новое вхождение блока, используя ранее сохраненный ID определения блока
                BlockReference br = new BlockReference(Point3d.Origin, btrId);

                // добавляем созданное вхождение блока на пространство модели и в транзакцию
                ms.AppendEntity(br);
                trans.AddNewlyCreatedDBObject(br, true);
                */
                return block_defenition;
            }
            catch 
            {
                return null;
            }

           

        }

        /// <summary>
        /// Создание выноски с блоком
        /// </summary>
        /// <param name="trans"></param>
        /// <param name="db"></param>
        /// <param name="ed"></param>
        public static void create_mleader(Transaction trans, Database db, Editor ed,BlockTableRecord block_name, string atribut_text,Point3d point_block, string name_layer)
        {
            BlockTable table = trans.GetObject(db.BlockTableId,OpenMode.ForRead) as BlockTable;
            BlockTableRecord model = trans.GetObject(table[BlockTableRecord.ModelSpace],OpenMode.ForWrite) as BlockTableRecord;
            /*
            if (!table.Has(block_name))

            {

                ed.WriteMessage($"Ошибка: не сущетсвует блока с именем {block_name}");

                return;

            }
            */
            MLeader leader = new MLeader();
            leader.SetDatabaseDefaults();
            leader.ContentType = ContentType.BlockContent;          
            leader.BlockContentId = block_name.ObjectId; //берет блок созданный программой


            leader.BlockPosition = new Point3d(point_block.X + 10, point_block.Y + 10,0);
            int idx = leader.AddLeaderLine(new Point3d(point_block.X, point_block.Y, 0));
            //leader.AddFirstVertex(idx, point_block);
            //Handle Block Attributes
            int AttNumber = 0;
            BlockTableRecord blkLeader = trans.GetObject(leader.BlockContentId,OpenMode.ForRead) as BlockTableRecord;
            //Doesn't take in consideration oLeader.BlockRotation
            Matrix3d Transfo = Matrix3d.Displacement(leader.BlockPosition.GetAsVector());
            foreach (ObjectId blkEntId in blkLeader)
            {
                AttributeDefinition AttributeDef = trans.GetObject(blkEntId,OpenMode.ForRead) as AttributeDefinition;
                if (AttributeDef != null)
                {
                    AttributeReference AttributeRef = new AttributeReference();
                    AttributeRef.SetAttributeFromBlock(AttributeDef,Transfo);
                    AttributeRef.Position =AttributeDef.Position.TransformBy(Transfo);
                    AttributeRef.TextString = atribut_text + (++AttNumber);
                    leader.Layer = name_layer; // помещает выноску на слой
                    leader.SetBlockAttribute(blkEntId, AttributeRef);
                }
                //else
                //{
                //    //если не находит атрибута, то изменить обычный текст.
                //    DBText text = trans.GetObject(blkEntId, OpenMode.ForWrite) as DBText;
                //    if(text != null)
                //    {
                //        text.TextString = atribut_text;
                //    }
                //}
            }
            model.AppendEntity(leader); // добавить сущность в модель
            trans.AddNewlyCreatedDBObject(leader, true); // добавить в транзакцию
        }
    }
}
