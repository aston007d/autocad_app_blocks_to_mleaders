﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Autodesk
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;


namespace AutoCAD_app_2021
{
    public class main : IExtensionApplication
    {
        [CommandMethod("blocks_to_mleaders")]
        public static void blocks_to_mleaders()

        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor; // это командная строка автокада
            //-----------------------------------------------------------------
            string blockName = "BlockMLeader01"; // имя которое будет присвоено созданному программой блоку, который потом вставится в ВЫНОСКУ
            string atribut_text = "attribut"; // текст, который будет записан в атрибут блока Выноски
            const string search_block_name = "LABEL"; // название блоков которые надо найти и заменить. Пример LABEL1, LABEL2 ... LABELn
            const string name_layer = "MLeader_layer";
            List<ObjectId> blocks_instances = new List<ObjectId>(); // сюда будет записана коллеция найденных блоков, которые нужно будет заменить на выноски
            // блокируем документ
            using (DocumentLock docloc = doc.LockDocument())
            {
                using (Transaction trans = db.TransactionManager.StartTransaction())
                {
                    try
                    {
                        blocks_instances = methods.search_blocks(trans, db, search_block_name);// находим коллекцию нужных блоков, которые надо заменить на выноски
                        BlockTableRecord block_defenition = methods.create_block(trans, db, blockName, ed, atribut_text); //создаем блок, который будет вставлен вместо текста в мультивыноску
                        if (block_defenition == null)
                        {
                            ed.WriteMessage($"При работе программы произошла ошибка: не удалось создать блок с именем {blockName}\n");
                            return;
                        }
                        
                        foreach (ObjectId id in blocks_instances)
                        {
                            BlockReference bref = trans.GetObject(id, OpenMode.ForRead) as BlockReference; // по ID получаем BlockReference
                            BlockTableRecord btr = (BlockTableRecord)trans.GetObject(bref.BlockTableRecord, OpenMode.ForRead); // теперь получаем экземляр блока
                            Point3d point_block = bref.Position; // точка вставки блока должна совпадать с точкой вставки выноски. 
                                                                 //Точка point_block Будет использвана для вставки выноски.

                            foreach (ObjectId id_Entity in btr) // проход по вложенным в блок элементам
                            {
                                if (id_Entity.ObjectClass.DxfName == "MTEXT")
                                {
                                    Entity entity = (Entity)trans.GetObject(id_Entity, OpenMode.ForRead);
                                    MText mytext = entity as MText;
                                    atribut_text = mytext.Text;
                                    break;
                                }

                            }
                            methods.create_layer(trans,db,name_layer,ed);
                            //теперь нужно вызывать метод создания мультивыноски и передать ей параметры
                            methods.create_mleader(trans, db, ed, block_defenition, atribut_text, point_block, name_layer); //создаем мультивыноску с переданным ей функцией create_block() блоком

                        }

                    }
                    catch (System.Exception ex)
                    {
                        trans.Abort();
                        ed.WriteMessage($"При работе программы произошла ошибка: \n {ex.Message} \n {ex.ToString()}");
                        return;
                    }

                    trans.Commit();
                }
            }

        }
       
        public void Initialize()
        {
            var editor = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument.Editor;
            editor.WriteMessage("Инициализация плагина.." + Environment.NewLine);
        }

        public void Terminate()
        {
            throw new NotImplementedException();
        }
    }
}
